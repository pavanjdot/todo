import React from 'react'




class List extends React.Component{
    state = {data: [], edit: false, newValue: '', change: false}
    
    change=()=>{
        this.setState({
            change: false,
            edit: false
        })
    }
    

    onEditingDone(e, id){
        
        if(e===13 ){
            
            const edited = {
                id: id,
                value: this.state.newValue
            };

            const newdata = [...this.state.data]
            newdata.push(edited)
            
            
            alert("Editing Done")

            this.setState({
                data: newdata,
                edit: true,
                newValue: "",
                change: true,
                
                
                
            });
            
            this.myInp.style.display='none'
            this.props.edit(this.state.newValue)

        
            
            
        }
        

        
    }

    onchanetask(changedvalue){
        this.setState({
            newValue: changedvalue
            
            
        })
       
    }

    delete = (id)=>{
        this.props.delete(id)
    }

    editTodo =()=>{
        this.setState({
            edit: true,
            
        })
    }

   


    render(){
       
        return(
            
            this.props.content.map(task => {
                return(
                    <div className="ui cards" key={task.id}>
                            <div className="card center" >
                                <div className="content ">
                                    <div className="header" onDoubleClick={this.editTodo}>
                                        <input type="checkbox" 
                                        style={{margin:10}}
                                        type="checkbox"  
                                        name="idDone" 
                                        className="check"
                                        checked={task.isDone}/>
                                      
                                     
                                     {this.state.change===true? this.state.data.map(task => {return task.value}):task.value }
                                     
                                     
                                     
                                        
                                    </div>
                                
                                </div>
                                <div className="ui inverted red button" 
                                     onClick={() => {this.delete(task.id)}}>
                                 
                                             Delete
                                    
                                </div>
                                {this.state.edit===true? <input className="ui error input" ref={(ele) => this.myInp = ele}
                                 value={this.state.newValue}
                                  onChange={e => {this.onchanetask(e.target.value)}} 
                                  onKeyDown={(e) =>this.onEditingDone(e.which, task.id)}
                                  />: ''}

                                {this.state.edit===true? <button onClick={this.change} className="ui inverted green button">Hit Enter & press this</button>: ''}
                                  
                                 
                               
                                
                            </div>

                            
                    </div>
                );
            })
        );
    }
    
}

export default List;