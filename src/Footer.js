import React from 'react'

var style = {
    backgroundColor: "#F8F8F8",
    borderTop: "1px solid #E7E7E7",
    textAlign: "center",
    padding: "20px",
    position: "fixed",
    left: "0",
    bottom: "0",
    height: "60px",
    width: "100%",
};

class Footer extends React.Component{
    render() {
        return (
            <div style={style}>
                © 2019 <a href="http://pavanj.com/">Pavan Joshi</a>, all rights reserved
            </div>
        );
    }

}

export default Footer;