import React from 'react'
import List from './List.js'
import Footer from './Footer.js'
import './App.css'

class App extends React.Component{
    state = {content: [], newItem: '', isAdded: false}

    editTodo = (newValue) => {
        console.log(newValue)
    }

    deleteItem = id => {    
        const content = [...this.state.content];
        const updatedlist = content.filter(item => item.id !== id);
        this.setState({ content: updatedlist });
      }

    changeState = (data)=>{
        this.setState({
            newItem: data
        })
    }


    handleClick = (item)=>{
        if(item !== ''){
            const itemObj={
                id: Date.now(),
                value: item,
                idDone: false
    
            };
    
            const content = [...this.state.content]
            content.push(itemObj)
    
            this.setState({
                content: content,
                newItem:'',
                isAdded: true
            });
        }
        else{
            alert("Please Add a Task")
        }
        
    }

    
    render(){
        return(
            <div className="center">
            <h2>TO-DO App</h2>
            <p>Add an item</p>
            <div className="ui icon input">
                <input type="text" 
                placeholder="Write a ToDo..." 
                style={{border: '0.5px solid blue'}}
                value={this.state.newItem}
                onChange={e=>this.changeState(e.target.value)}
                />
                <i className="tasks icon"></i>    
            </div>
            <button className="ui primary basic button" 
              onClick={()=>this.handleClick(this.state.newItem)} >Add
            </button>

            {this.state.isAdded===true? <List content={this.state.content} edit={this.editTodo} delete={this.deleteItem}/>: ''}
            <Footer/>

            
             
            
        </div>

        );
    }
    
}

export default App; 